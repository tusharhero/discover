# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-20 01:35+0000\n"
"PO-Revision-Date: 2015-02-04 16:18+0000\n"
"Last-Translator: Nermina Ahmić <nahmic1@etf.unsa.ba>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Launchpad-Export-Date: 2015-02-05 06:39+0000\n"
"X-Generator: Launchpad (build 17331)\n"

#: discover/DiscoverObject.cpp:190
#, kde-format
msgctxt "@title %1 is the distro name"
msgid ""
"%1 is not configured for installing apps through Discover—only app add-ons"
msgstr ""

#: discover/DiscoverObject.cpp:192
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the distro name"
msgid ""
"To use Discover for apps, install your preferred module on the "
"<interface>Settings</interface> page, under <interface>Missing Backends</"
"interface>."
msgstr ""

#: discover/DiscoverObject.cpp:195
#, kde-format
msgctxt "@action:button %1 is the distro name"
msgid "Report This Issue to %1"
msgstr ""

#: discover/DiscoverObject.cpp:200
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is the distro name; in this case it always contains 'Arch "
"Linux'"
msgid ""
"To use Discover for apps, install <link url='https://wiki.archlinux.org/"
"title/Flatpak#Installation'>Flatpak</link> or <link url='https://wiki."
"archlinux.org/title/KDE#Discover_does_not_show_any_applications'>PackageKit</"
"link> using the <command>pacman</command> package manager.<nl/><nl/> Review "
"<link url='https://archlinux.org/packages/extra/x86_64/discover/'>%1's "
"packaging for Discover</link>"
msgstr ""

#: discover/DiscoverObject.cpp:291
#, kde-format
msgid "Could not find category '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:328
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""

#: discover/DiscoverObject.cpp:332
#, kde-format
msgid "Could not open %1"
msgstr ""

#: discover/DiscoverObject.cpp:394
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr ""

#: discover/DiscoverObject.cpp:396
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""

#: discover/DiscoverObject.cpp:399
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:402
#, kde-format
msgid "Report This Issue"
msgstr ""

#: discover/DiscoverObject.cpp:464 discover/DiscoverObject.cpp:466
#: discover/main.cpp:117
#, kde-format
msgid "Discover"
msgstr "Otkrij"

#: discover/DiscoverObject.cpp:467
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""

#: discover/main.cpp:33
#, fuzzy, kde-format
#| msgid "Directly open the specified application by its package name."
msgid "Directly open the specified application by its appstream:// URI."
msgstr "Direktno otvori navedenu aplikaciju njenim imenom paketa."

#: discover/main.cpp:34
#, fuzzy, kde-format
#| msgid "Open with a program that can deal with the given mimetype."
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "Otvori programom koji može raditi s datim MIME tipom."

#: discover/main.cpp:35
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Prikaži listu elemenata s kategorijom."

#: discover/main.cpp:36
#, fuzzy, kde-format
#| msgid ""
#| "Open Muon Discover in a said mode. Modes correspond to the toolbar "
#| "buttons."
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""
"Otvori Muon otkrivač u datom režimu. Režimi odgovaraju dugmićima alatne "
"trake."

#: discover/main.cpp:37
#, kde-format
msgid "List all the available modes."
msgstr "Nabroji sve dostupne režime rada."

#: discover/main.cpp:38
#, kde-format
msgid "Local package file to install"
msgstr ""

#: discover/main.cpp:39
#, kde-format
msgid "List all the available backends."
msgstr "Nabroji sve dostupne pozadinske programe."

#: discover/main.cpp:40
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search string."
msgstr "Tražim u  '%1'..."

#: discover/main.cpp:41
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: discover/main.cpp:43
#, kde-format
msgid "Supports appstream: url scheme"
msgstr ""

#: discover/main.cpp:119
#, fuzzy, kde-format
msgid "An application explorer"
msgstr "Otkrivač programa"

#: discover/main.cpp:121
#, kde-format
msgid "© 2010-2024 Plasma Development Team"
msgstr ""

#: discover/main.cpp:122
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:123
#, kde-format
msgid "Nate Graham"
msgstr ""

#: discover/main.cpp:124
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr ""

#: discover/main.cpp:128
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr ""

#: discover/main.cpp:129
#, kde-format
msgid "KNewStuff"
msgstr ""

#: discover/main.cpp:136
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: discover/main.cpp:136
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: discover/main.cpp:149
#, kde-format
msgid "Available backends:\n"
msgstr "Dostupni pozadinski programi:\n"

#: discover/main.cpp:202
#, kde-format
msgid "Available modes:\n"
msgstr "Dostupni režimi:\n"

#: discover/qml/AddonsView.qml:20 discover/qml/Navigation.qml:58
#, kde-format
msgid "Addons for %1"
msgstr ""

#: discover/qml/AddonsView.qml:56
#, kde-format
msgid "More…"
msgstr ""

#: discover/qml/AddonsView.qml:65
#, kde-format
msgid "Apply Changes"
msgstr ""

#: discover/qml/AddonsView.qml:72
#, kde-format
msgid "Reset"
msgstr ""

#: discover/qml/AddSourceDialog.qml:21
#, kde-format
msgid "Add New %1 Repository"
msgstr ""

#: discover/qml/AddSourceDialog.qml:45
#, fuzzy, kde-format
#| msgid "Add-ons"
msgid "Add"
msgstr "Dodaci"

#: discover/qml/AddSourceDialog.qml:50 discover/qml/DiscoverWindow.qml:271
#: discover/qml/InstallApplicationButton.qml:46
#: discover/qml/ProgressView.qml:139 discover/qml/SourcesPage.qml:201
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Otkaži"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, fuzzy, kde-format
#| msgid "Rating"
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "Rejting"
msgstr[1] "Rejting"
msgstr[2] "Rejting"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, kde-format
msgid "No ratings yet"
msgstr ""

#: discover/qml/ApplicationPage.qml:67
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr ""

#: discover/qml/ApplicationPage.qml:84
#, kde-format
msgid "%1 - %2"
msgstr ""

#: discover/qml/ApplicationPage.qml:203
#, kde-format
msgid "Unknown author"
msgstr ""

#: discover/qml/ApplicationPage.qml:248
#, kde-format
msgid "Version:"
msgstr ""

#: discover/qml/ApplicationPage.qml:260
#, fuzzy, kde-format
msgid "Size:"
msgstr "<b>Ukupna veličina:</b> %1<br/>"

#: discover/qml/ApplicationPage.qml:272
#, fuzzy, kde-format
#| msgid "points: %1"
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "poena: %1"
msgstr[1] "poena: %1"
msgstr[2] "poena: %1"

#: discover/qml/ApplicationPage.qml:280
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr ""

#: discover/qml/ApplicationPage.qml:314
#, kde-format
msgid "What does this mean?"
msgstr ""

#: discover/qml/ApplicationPage.qml:323
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: discover/qml/ApplicationPage.qml:334
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Content Rating:"
msgstr "Rejting"

#: discover/qml/ApplicationPage.qml:343
#, kde-format
msgid "Age: %1+"
msgstr ""

#: discover/qml/ApplicationPage.qml:363
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr ""

#: discover/qml/ApplicationPage.qml:385
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr ""

#: discover/qml/ApplicationPage.qml:559
#, kde-format
msgid "Documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:560
#, kde-format
msgid "Read the project's official documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:576
#, kde-format
msgid "Website"
msgstr ""

#: discover/qml/ApplicationPage.qml:577
#, kde-format
msgid "Visit the project's website"
msgstr ""

#: discover/qml/ApplicationPage.qml:593
#, kde-format
msgid "Addons"
msgstr ""

#: discover/qml/ApplicationPage.qml:594
#, kde-format
msgid "Install or remove additional functionality"
msgstr ""

#: discover/qml/ApplicationPage.qml:613
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr ""

#: discover/qml/ApplicationPage.qml:614
#, kde-format
msgid "Send a link for this application"
msgstr ""

#: discover/qml/ApplicationPage.qml:630
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:650
#, kde-format
msgid "What's New"
msgstr ""

#: discover/qml/ApplicationPage.qml:680
#, kde-format
msgid "Reviews"
msgstr "Recenzije"

#: discover/qml/ApplicationPage.qml:692
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Loading reviews for %1"
msgstr "Rejting"

#: discover/qml/ApplicationPage.qml:700
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr ""

#: discover/qml/ApplicationPage.qml:724
#, fuzzy, kde-format
#| msgid "%1 reviews"
msgctxt "@action:button"
msgid "Show All Reviews"
msgstr "%1 recenzija"

#: discover/qml/ApplicationPage.qml:736
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write a Review"
msgstr "Predaj"

#: discover/qml/ApplicationPage.qml:736
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Install to Write a Review"
msgstr "Predaj"

#: discover/qml/ApplicationPage.qml:748
#, kde-format
msgid "Get Involved"
msgstr ""

#: discover/qml/ApplicationPage.qml:790
#, kde-format
msgid "Donate"
msgstr ""

#: discover/qml/ApplicationPage.qml:791
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""

#: discover/qml/ApplicationPage.qml:807
#, kde-format
msgid "Report Bug"
msgstr ""

#: discover/qml/ApplicationPage.qml:808
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr ""

#: discover/qml/ApplicationPage.qml:824
#, kde-format
msgid "Contribute"
msgstr ""

#: discover/qml/ApplicationPage.qml:825
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:857
#, fuzzy, kde-format
#| msgid "points: %1"
msgid "All Licenses"
msgstr "poena: %1"

#: discover/qml/ApplicationPage.qml:890
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Content Rating"
msgstr "Rejting"

#: discover/qml/ApplicationPage.qml:907
#, kde-format
msgid "Risks of proprietary software"
msgstr ""

#: discover/qml/ApplicationPage.qml:913
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""

#: discover/qml/ApplicationPage.qml:914
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:53
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Tražim u  '%1'..."
msgstr[1] "Tražim u  '%1'..."
msgstr[2] "Tražim u  '%1'..."

#: discover/qml/ApplicationsListPage.qml:55
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search: %1"
msgstr "Tražim u  '%1'..."

#: discover/qml/ApplicationsListPage.qml:59
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "Tražim u  '%1'..."
msgstr[1] "Tražim u  '%1'..."
msgstr[2] "Tražim u  '%1'..."

#: discover/qml/ApplicationsListPage.qml:65
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Tražim u  '%1'..."
msgstr[1] "Tražim u  '%1'..."
msgstr[2] "Tražim u  '%1'..."

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:254
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search"
msgstr "Traži..."

#: discover/qml/ApplicationsListPage.qml:98 discover/qml/ReviewsPage.qml:99
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Sort: %1"
msgstr "Tražim u  '%1'..."

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgctxt "Search results most relevant to the search query"
msgid "Relevance"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:114
#, kde-format
msgid "Name"
msgstr "Ime"

#: discover/qml/ApplicationsListPage.qml:125 discover/qml/Rating.qml:119
#, kde-format
msgid "Rating"
msgstr "Rejting"

#: discover/qml/ApplicationsListPage.qml:136
#, fuzzy, kde-format
msgid "Size"
msgstr "<b>Ukupna veličina:</b> %1<br/>"

#: discover/qml/ApplicationsListPage.qml:147
#, kde-format
msgid "Release Date"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:200
#, kde-format
msgid "Nothing found"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:218
#, fuzzy, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Navedi novi izvor"

#: discover/qml/ApplicationsListPage.qml:222
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:233
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:235
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:236
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:269
#, fuzzy, kde-format
#| msgid "Install"
msgid "Still looking…"
msgstr "Instaliraj"

#: discover/qml/BrowsingPage.qml:20
#, fuzzy, kde-format
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "Početna stranica"

#: discover/qml/BrowsingPage.qml:64
#, kde-format
msgid "Unable to load applications"
msgstr ""

#: discover/qml/BrowsingPage.qml:98
#, fuzzy, kde-format
#| msgid "Popularity"
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Popularnost"

#: discover/qml/BrowsingPage.qml:118
#, kde-format
msgctxt "@title:group"
msgid "Newly Published & Recently Updated"
msgstr ""

#: discover/qml/BrowsingPage.qml:155
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr ""

#: discover/qml/BrowsingPage.qml:170
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr ""

#: discover/qml/BrowsingPage.qml:189 discover/qml/BrowsingPage.qml:218
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr ""

#: discover/qml/BrowsingPage.qml:199
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr ""

#: discover/qml/CarouselDelegate.qml:212
#, kde-format
msgctxt "@action:button Start playing media"
msgid "Play"
msgstr ""

#: discover/qml/CarouselDelegate.qml:214
#, kde-format
msgctxt "@action:button Pause any media that is playing"
msgid "Pause"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:40
#, kde-format
msgctxt "@action:button"
msgid "Switch to Overlay"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Switch to Full Screen"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:75
#, fuzzy, kde-format
#| msgid "Close"
msgctxt ""
"@action:button Close overlay/window/popup with carousel of screenshots"
msgid "Close"
msgstr "Zatvori"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Previous Screenshot"
msgstr ""

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Next Screenshot"
msgstr ""

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr ""

#: discover/qml/DiscoverWindow.qml:58
#, fuzzy, kde-format
msgid "&Home"
msgstr "Početna stranica"

#: discover/qml/DiscoverWindow.qml:68
#, fuzzy, kde-format
#| msgid "Search..."
msgid "&Search"
msgstr "Traži..."

#: discover/qml/DiscoverWindow.qml:76
#, fuzzy, kde-format
#| msgid "Installed"
msgid "&Installed"
msgstr "Instalirano"

#: discover/qml/DiscoverWindow.qml:87
#, fuzzy, kde-format
#| msgid "Update"
msgid "Fetching &updates…"
msgstr "Ažuriraj"

#: discover/qml/DiscoverWindow.qml:87
#, fuzzy, kde-format
#| msgid "Update"
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "Ažuriraj"
msgstr[1] "Ažuriraj"
msgstr[2] "Ažuriraj"

#: discover/qml/DiscoverWindow.qml:95
#, kde-format
msgid "&About"
msgstr ""

#: discover/qml/DiscoverWindow.qml:103
#, fuzzy, kde-format
#| msgid "Best Ratings"
msgid "S&ettings"
msgstr "Najbolji plasmani"

#: discover/qml/DiscoverWindow.qml:156 discover/qml/DiscoverWindow.qml:340
#: discover/qml/DiscoverWindow.qml:454
#, kde-format
msgid "Error"
msgstr ""

#: discover/qml/DiscoverWindow.qml:160
#, kde-format
msgid "Unable to find resource: %1"
msgstr ""

#: discover/qml/DiscoverWindow.qml:258 discover/qml/SourcesPage.qml:195
#, kde-format
msgid "Proceed"
msgstr ""

#: discover/qml/DiscoverWindow.qml:316
#, kde-format
msgid "Report this issue"
msgstr ""

#: discover/qml/DiscoverWindow.qml:340
#, kde-format
msgid "Error %1 of %2"
msgstr ""

#: discover/qml/DiscoverWindow.qml:390
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr ""

#: discover/qml/DiscoverWindow.qml:403
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr ""

#: discover/qml/DiscoverWindow.qml:419
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: discover/qml/Feedback.qml:14
#, kde-format
msgid "Submit usage information"
msgstr ""

#: discover/qml/Feedback.qml:15
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Submitting usage information…"
msgstr ""

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Configure"
msgstr ""

#: discover/qml/Feedback.qml:23
#, kde-format
msgid "Configure feedback…"
msgstr ""

#: discover/qml/Feedback.qml:30 discover/qml/SourcesPage.qml:22
#, fuzzy, kde-format
#| msgid "Update"
msgid "Configure Updates…"
msgstr "Ažuriraj"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr ""

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr ""

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr ""

#: discover/qml/InstallApplicationButton.qml:24
#, fuzzy, kde-format
#| msgid "Loading..."
msgctxt "State being fetched"
msgid "Loading…"
msgstr "Učitavam..."

#: discover/qml/InstallApplicationButton.qml:28
#, fuzzy, kde-format
#| msgid "Install"
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Instaliraj"

#: discover/qml/InstallApplicationButton.qml:30
#, fuzzy, kde-format
#| msgid "Install"
msgctxt "@action:button"
msgid "Install"
msgstr "Instaliraj"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Ukloni"

#: discover/qml/InstalledPage.qml:14
#, kde-format
msgid "Installed"
msgstr "Instalirano"

#: discover/qml/Navigation.qml:34
#, kde-format
msgid "Resources for '%1'"
msgstr ""

#: discover/qml/ProgressView.qml:18
#, fuzzy, kde-format
#| msgid "Update"
msgid "Tasks (%1%)"
msgstr "Ažuriraj"

#: discover/qml/ProgressView.qml:18 discover/qml/ProgressView.qml:45
#, kde-format
msgid "Tasks"
msgstr ""

#: discover/qml/ProgressView.qml:113
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr ""

#: discover/qml/ProgressView.qml:121
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr ""

#: discover/qml/ProgressView.qml:128
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr ""

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "unknown reviewer"
msgstr ""

#: discover/qml/ReviewDelegate.qml:66
#, fuzzy, kde-format
#| msgid "<b>%1</b><br/>%2"
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b><br/>%2"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "Comment by %1"
msgstr ""

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: %1"
msgstr ""

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: unknown"
msgstr ""

#: discover/qml/ReviewDelegate.qml:100
#, kde-format
msgid "Votes: %1 out of %2"
msgstr ""

#: discover/qml/ReviewDelegate.qml:107
#, kde-format
msgid "Was this review useful?"
msgstr ""

#: discover/qml/ReviewDelegate.qml:119
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr ""

#: discover/qml/ReviewDelegate.qml:136
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr ""

#: discover/qml/ReviewDialog.qml:20
#, fuzzy, kde-format
#| msgid "Reviewing %1"
msgid "Reviewing %1"
msgstr "Recenziram %1"

#: discover/qml/ReviewDialog.qml:27
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Submit review"
msgstr "Predaj"

#: discover/qml/ReviewDialog.qml:40
#, kde-format
msgid "Rating:"
msgstr "Ocjena:"

#: discover/qml/ReviewDialog.qml:45
#, fuzzy, kde-format
#| msgid "Name"
msgid "Name:"
msgstr "Ime"

#: discover/qml/ReviewDialog.qml:53
#, kde-format
msgid "Title:"
msgstr ""

#: discover/qml/ReviewDialog.qml:73
#, kde-format
msgid "Enter a rating"
msgstr ""

#: discover/qml/ReviewDialog.qml:76
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write the title"
msgstr "Predaj"

#: discover/qml/ReviewDialog.qml:79
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write the review"
msgstr "Predaj"

#: discover/qml/ReviewDialog.qml:82
#, fuzzy, kde-format
#| msgid "Loading..."
msgid "Keep writing…"
msgstr "Učitavam..."

#: discover/qml/ReviewDialog.qml:85
#, kde-format
msgid "Too long!"
msgstr ""

#: discover/qml/ReviewDialog.qml:88
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr ""

#: discover/qml/ReviewsPage.qml:54
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Reviews for %1"
msgstr "Rejting"

#: discover/qml/ReviewsPage.qml:62
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write a Review…"
msgstr "Predaj"

#: discover/qml/ReviewsPage.qml:74
#, kde-format
msgid "Install this app to write a review"
msgstr ""

#: discover/qml/ReviewsPage.qml:103
#, kde-format
msgctxt "@label:listbox Most relevant reviews"
msgid "Most Relevant"
msgstr ""

#: discover/qml/ReviewsPage.qml:110
#, kde-format
msgctxt "@label:listbox Most recent reviews"
msgid "Most Recent"
msgstr ""

#: discover/qml/ReviewsPage.qml:117
#, fuzzy, kde-format
#| msgid "Rating"
msgctxt "@label:listbox Reviews with the highest ratings"
msgid "Highest Ratings"
msgstr "Rejting"

#: discover/qml/ReviewsStats.qml:53
#, fuzzy, kde-format
#| msgid "Reviews"
msgctxt "how many reviews"
msgid "%1 reviews"
msgstr "Recenzije"

#: discover/qml/ReviewsStats.qml:76
#, kde-format
msgctxt "widest character in the language"
msgid "M"
msgstr ""

#: discover/qml/ReviewsStats.qml:154
#, kde-format
msgid "Unknown reviewer"
msgstr ""

#: discover/qml/ReviewsStats.qml:175
#, kde-format
msgctxt "Opening upper air quote"
msgid "“"
msgstr ""

#: discover/qml/ReviewsStats.qml:190
#, kde-format
msgctxt "Closing lower air quote"
msgid "„"
msgstr ""

#: discover/qml/SearchField.qml:26
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search…"
msgstr "Traži..."

#: discover/qml/SearchField.qml:26
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search in '%1'…"
msgstr "Tražim u  '%1'..."

#: discover/qml/SourcesPage.qml:18
#, fuzzy, kde-format
#| msgid "Best Ratings"
msgid "Settings"
msgstr "Najbolji plasmani"

#: discover/qml/SourcesPage.qml:110
#, fuzzy, kde-format
#| msgid "Sources"
msgid "Default source"
msgstr "Izvori"

#: discover/qml/SourcesPage.qml:118
#, fuzzy, kde-format
#| msgid "Add Source"
msgid "Add Source…"
msgstr "Dodaj izvor"

#: discover/qml/SourcesPage.qml:145
#, kde-format
msgid "Make default"
msgstr ""

#: discover/qml/SourcesPage.qml:248
#, kde-format
msgid "Increase priority"
msgstr ""

#: discover/qml/SourcesPage.qml:254
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:260
#, kde-format
msgid "Decrease priority"
msgstr ""

#: discover/qml/SourcesPage.qml:266
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:272
#, kde-format
msgid "Remove repository"
msgstr ""

#: discover/qml/SourcesPage.qml:283
#, kde-format
msgid "Show contents"
msgstr ""

#: discover/qml/SourcesPage.qml:324
#, kde-format
msgid "Missing Backends"
msgstr ""

#: discover/qml/UpdatesPage.qml:13
#, fuzzy, kde-format
#| msgid "Update"
msgid "Updates"
msgstr "Ažuriraj"

#: discover/qml/UpdatesPage.qml:46
#, fuzzy, kde-format
#| msgid "Update"
msgid "Update Issue"
msgstr "Ažuriraj"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Technical details"
msgstr ""

#: discover/qml/UpdatesPage.qml:62
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""

#: discover/qml/UpdatesPage.qml:68
#, kde-format
msgid "See Technical Details"
msgstr ""

#: discover/qml/UpdatesPage.qml:95
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""

#: discover/qml/UpdatesPage.qml:103
#, kde-format
msgid "Copy Text"
msgstr ""

#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr ""

#: discover/qml/UpdatesPage.qml:114
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr ""

#: discover/qml/UpdatesPage.qml:140
#, fuzzy, kde-format
#| msgid "Update All"
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Ažuriraj sve"

#: discover/qml/UpdatesPage.qml:140
#, fuzzy, kde-format
#| msgid "Update All"
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Ažuriraj sve"

#: discover/qml/UpdatesPage.qml:180
#, kde-format
msgid "Ignore"
msgstr ""

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr ""

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr ""

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr ""

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr ""

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr ""

#: discover/qml/UpdatesPage.qml:413
#, fuzzy, kde-format
#| msgid "Install"
msgid "Installing"
msgstr "Instaliraj"

#: discover/qml/UpdatesPage.qml:444
#, fuzzy, kde-format
#| msgid "Update All"
msgid "Update from:"
msgstr "Ažuriraj sve"

#: discover/qml/UpdatesPage.qml:456
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr ""

#: discover/qml/UpdatesPage.qml:463
#, kde-format
msgid "More Information…"
msgstr ""

#: discover/qml/UpdatesPage.qml:491
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Fetching updates…"
msgstr "Ažuriraj"

#: discover/qml/UpdatesPage.qml:504
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Updates"
msgstr "Ažuriraj"

#: discover/qml/UpdatesPage.qml:513
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr ""

#: discover/qml/UpdatesPage.qml:525 discover/qml/UpdatesPage.qml:532
#: discover/qml/UpdatesPage.qml:539
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Up to date"
msgstr "Ažuriraj"

#: discover/qml/UpdatesPage.qml:546
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr ""

#: discover/qml/UpdatesPage.qml:553
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr ""

#, fuzzy
#~| msgid "Update"
#~ msgid "&Up to date"
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Sources"
#~ msgid "Sources"
#~ msgstr "Izvori"

#, fuzzy
#~| msgid "Loading..."
#~ msgid "Loading…"
#~ msgstr "Učitavam..."

#, fuzzy
#~| msgid "Sources"
#~ msgid "Source:"
#~ msgstr "Izvori"

#, fuzzy
#~| msgid "Update All"
#~ msgid "All updates selected (%1)"
#~ msgstr "Ažuriraj sve"

#, fuzzy
#~| msgid "Update All"
#~ msgid "%1/%2 update selected (%3)"
#~ msgid_plural "%1/%2 updates selected (%3)"
#~ msgstr[0] "Ažuriraj sve"
#~ msgstr[1] "Ažuriraj sve"
#~ msgstr[2] "Ažuriraj sve"

#~ msgid "OK"
#~ msgstr "U redu"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "State being fetched"
#~ msgid "Loading..."
#~ msgstr "Učitavam..."

#, fuzzy
#~| msgid "Submit"
#~ msgid "Write a Review..."
#~ msgstr "Predaj"

#~ msgid "Search..."
#~ msgstr "Traži..."

#, fuzzy
#~| msgid "Submit"
#~ msgid "Write a review!"
#~ msgstr "Predaj"

#~ msgid "Jonathan Thomas"
#~ msgstr "Jonathan Thomas"

#~ msgid "Discard"
#~ msgstr "Odbaci"

#, fuzzy
#~ msgid "Homepage:"
#~ msgstr "Početna stranica"

#, fuzzy
#~| msgid "Update"
#~ msgid "Fetching Updates..."
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Fetching Updates..."
#~ msgstr "Ažuriraj"

#~ msgid "<em>Tell us about this review!</em>"
#~ msgstr "<em>Recite nam o ovoj recenziji!</em>"

#~ msgid "<em>%1 out of %2 people found this review useful</em>"
#~ msgstr "<em>%1 od %2 ljudi smatra ovu recenziju korisnom</em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em>"
#~ msgstr ""
#~ "<em>Korisno? <a href='true'><b>Da</b></a>/<a href='false'>Ne</a></em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em>"
#~ msgstr ""
#~ "<em>Korisno? <a href='true'>Da</a>/<a href='false'><b>Ne</b></a></em>"

#~ msgid "<em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em>"
#~ msgstr "<em>Korisno? <a href='true'>Da</a>/<a href='false'>Ne</a></em>"

#, fuzzy
#~| msgid "Review"
#~ msgid "Review:"
#~ msgstr "Pregled"

#, fuzzy
#~| msgid "Review"
#~ msgid "Review..."
#~ msgstr "Pregled"

#, fuzzy
#~| msgid "Update"
#~ msgid "Checking for updates..."
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Update"
#~ msgid "No Updates"
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Fetching..."
#~ msgstr "Učitavam..."

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Checking for updates..."
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Updating..."
#~ msgstr "Učitavam..."

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "No updates"
#~ msgstr "Ažuriraj"

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Looking for updates"
#~ msgstr "Ažuriraj"

#~ msgid "Summary:"
#~ msgstr "Sažetak:"

#~ msgid "Back"
#~ msgstr "Nazad"

#~ msgid "Launch"
#~ msgstr "Pokreni"

#, fuzzy
#~ msgid "Application Sources"
#~ msgstr "Otkrivač programa"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Loading..."
#~ msgstr "Učitavam..."

#~ msgid ""
#~ "<p style='margin: 0 0 0 0'><b>%1</b> by %2</p><p style='margin: 0 0 0 0'>"
#~ "%3</p><p style='margin: 0 0 0 0'>%4</p>"
#~ msgstr ""
#~ "<p style='margin: 0 0 0 0'><b>%1</b> by %2</p><p style='margin: 0 0 0 0'>"
#~ "%3</p><p style='margin: 0 0 0 0'>%4</p>"

#~ msgid "Popularity"
#~ msgstr "Popularnost"

#~ msgid "Buzz"
#~ msgstr "Zvrrr"

#~ msgid "Origin"
#~ msgstr "Porijeklo"

#~ msgid "List"
#~ msgstr "Lista"

#~ msgid "Grid"
#~ msgstr "Mreža"

#, fuzzy
#~| msgid "Installed"
#~ msgid "items installed"
#~ msgstr "Instalirano"

#, fuzzy
#~| msgid "Update"
#~ msgid "System Update"
#~ msgstr "Ažuriraj"

#~ msgid ""
#~ "Found some errors while setting up the GUI, the application can't proceed."
#~ msgstr ""
#~ "Nađene greške pri postavljanju grafičkog okruženja, aplikacija ne može "
#~ "nastaviti."

#~ msgid "Initialization error"
#~ msgstr "Greška inicijalizacije"

#~ msgid "Muon Discover"
#~ msgstr "Muon Otkrivač"

#~ msgid ""
#~ "Welcome to\n"
#~ "Muon Discover!"
#~ msgstr ""
#~ "Dobrodošli u\n"
#~ "Muon Otkrivač"

#~ msgid "Configure and learn about Muon Discover"
#~ msgstr "Konfiguriši i nauči Muon otkrivač"

#~ msgid "Installed (%1 update)"
#~ msgid_plural "Installed (%1 updates)"
#~ msgstr[0] "Instalirano (%1 nadogradnja)"
#~ msgstr[1] "Instalirano (%1 nadogradnje)"
#~ msgstr[2] "Instalirano (%1 nadogradnji)"

#~ msgid "Menu"
#~ msgstr "Meni"

#~ msgid "©2010-2012 Jonathan Thomas"
#~ msgstr "©2010-2012 Jonathan Thomas"

#~ msgid ""
#~ "List all the backends we'll want to have loaded, separated by coma ','."
#~ msgstr ""
#~ "Nabroji sve pozadinske programe koje ćemo učitavati, razdvojene zarezima "
#~ "','."

#~ msgid "<qt>%1<br/><em>%2</em></qt>"
#~ msgstr "<qt>%1<br/><em>%2</em></qt>"

#~ msgid "Apply"
#~ msgstr "Primijeni"

#~ msgid "Overview"
#~ msgstr "Pregled"

#~ msgid "Popularity Contest"
#~ msgstr "Takmičenje popularnosti"

#~ msgid "<b>Reviews:</b>"
#~ msgstr "<b>Recenzije:</b>"

#~ msgid ""
#~ "<sourceline> - The apt repository source line to add. This is one of:\n"
#~ msgstr ""
#~ "<sourceline> - Izvorna linija apt repozitorija koja se dodaje. Jedna od:\n"

#~ msgid "%1 (Binary)"
#~ msgstr "%1 (Binarno)"

#~ msgid "%1. %2"
#~ msgstr "%1. %2"
